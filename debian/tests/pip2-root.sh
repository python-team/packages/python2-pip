#!/bin/sh

set -eux

export PIP_DISABLE_PIP_VERSION_CHECK=1

python2.7 -m pip install 'world<4'
python2.7 -m pip list --format=columns
ls -ld /usr/local/lib/python2.7/dist-packages/world-*.dist-info
python2.7 -m pip uninstall -y world
python2.7 -m pip list --format=columns
